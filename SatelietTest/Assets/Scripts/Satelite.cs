﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Satelite : MonoBehaviour {

    private SateliteValues values = new SateliteValues(10, 0.5f, 0.005f);
    int turningDirection = 1;//0 == Counter Clockwise -> 1 == Clockwise
    List<Vector3> launchRoute;
    GameObject pivot;

    public void Spawn(List<Vector3> launchRoute)
    {
        this.launchRoute = launchRoute;
        pivot = new GameObject();
        transform.parent = pivot.transform;

        transform.position = launchRoute[0];
        DetermineDirection();
    }

    public SateliteValues GetValues()
    {
        return this.values;
    }

    private void Update()
    {
        if(launchRoute.Count != 0)
        {
            TravelToInitialDestination();
            return;
        }
        pivot.transform.Rotate(new Vector3(0, 0, this.values.OrbitalVelocity * turningDirection * -1));
    }

    void DetermineDirection()
    {
        Vector2 deltaBetweenLast = new Vector2(launchRoute[launchRoute.Count - 1].x - launchRoute[launchRoute.Count - 2].x,
                                              launchRoute[launchRoute.Count - 1].y - launchRoute[launchRoute.Count - 2].y);

        if (launchRoute[launchRoute.Count - 1].x > 0 && launchRoute[launchRoute.Count - 1].y > 0)
        {
            if (Mathf.Abs(deltaBetweenLast.y )> Mathf.Abs(deltaBetweenLast.x))
            {
                turningDirection = deltaBetweenLast.x > 0 ? 1 : -1;
            }
            else
            {
                turningDirection = deltaBetweenLast.y > 0 ? -1 : 1;
            }
        }
        else if (launchRoute[launchRoute.Count - 1].x < 0 && launchRoute[launchRoute.Count - 1].y > 0)
        {
            if (Mathf.Abs(deltaBetweenLast.y) > Mathf.Abs(deltaBetweenLast.x))
            {
                turningDirection = deltaBetweenLast.x > 0 ? 1 : -1;
            }
            else
            {
                turningDirection = deltaBetweenLast.y > 0 ? 1 : -1;
            }
        }
        else if (launchRoute[launchRoute.Count - 1].x < 0 && launchRoute[launchRoute.Count - 1].y < 0)
        {
            if (Mathf.Abs(deltaBetweenLast.y) > Mathf.Abs(deltaBetweenLast.x))
            {
                turningDirection = deltaBetweenLast.x > 0 ? -1 : 1;
            }
            else
            {
                turningDirection = deltaBetweenLast.y > 0 ? 1 : -1;
            }
        }
        else //(deltaBetweenLast.x > 0 && deltaBetweenLast.y < 0)
        {
            if (Mathf.Abs(deltaBetweenLast.y) > Mathf.Abs(deltaBetweenLast.x))
            {
                turningDirection = deltaBetweenLast.x > 0 ? -1 : 1;
            }
            else
            {
                turningDirection = deltaBetweenLast.y > 0 ? -1 : 1;
            }
        }

    }

    void TravelToInitialDestination()
    {
      
        float distanceTraveled = 0;
        distanceTraveled += Vector3.Distance(transform.position, launchRoute[0]);
        if(distanceTraveled > values.MaxTrajectoryLength)
        {
            Vector3 newPosition1 = Vector3.MoveTowards(transform.position, launchRoute[0], (values.MaxTrajectoryLength));
            transform.position = newPosition1;
            return;
        }
        while (distanceTraveled < values.MaxTrajectoryLength && launchRoute.Count > 0)
        {
            Vector3 newPosition = Vector3.MoveTowards(transform.position, launchRoute[0], (values.MaxTrajectoryLength - distanceTraveled));
            transform.position = newPosition;
            distanceTraveled += Vector3.Distance(transform.position, launchRoute[0]);


            if (transform.position == launchRoute[0])
                launchRoute.RemoveAt(0);
        }


    }

}
